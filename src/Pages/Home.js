import React from 'react';
import Navbar from '../Components/Navbar';
import './Home.css';

const Home = () => {
  return (
    <div>
      {/* Navbar */}
      <Navbar />

      {/* Hero Section */}
      <section className="hero">
        <div className="hero__content">
          <div className="hero-text"><h1>InternNexus</h1>
          <p>Find your dream internship and jumpstart your career.</p>
          <button>Get Started</button></div>
        </div>
      </section>

      {/* About Section */}
      <section className="about">
        <div className="about__content">
          <h2>About Us</h2>
          <p>
            We connect students and graduates with internships at top
            companies worldwide. Our platform helps you find the perfect
            opportunity to gain valuable experience and build your
            professional network.
          </p>
        </div>
      </section>

      {/* Footer */}
      <footer className="footer">
        
      </footer>
    </div>
  );
};

export default Home;