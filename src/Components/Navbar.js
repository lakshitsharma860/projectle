import React from 'react';
import './Navbar.css';

const Navbar = () => {
  return (
    <nav className="navbar">
      <div className="navbar__logo">
        {/* Your logo or branding goes here */}
         <img src="https://beta-test.internnexus.com/assets/White.png" alt="INTERN NEXUS" /> 
        
      </div>
      <ul className="navbar__links">
        <li className="navbar__item">
          <a href="/">Home</a>
        </li>
        <li className="navbar__item">
          <a href="/about">About</a>
        </li>
        <li className="navbar__item">
          <a href="/contact">Contact</a>
        </li>
        {/* Add more navigation links as needed */}
      </ul>
    </nav>
  );
};

export default Navbar;
