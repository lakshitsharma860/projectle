import './App.css';
import Home from './Pages/Home';
import Navbar from './Components/Navbar';

function App() {
  return (
    <div >
      <Home/>
      <Navbar/>
    </div>
  );
}

export default App;
